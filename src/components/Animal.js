import { Component } from "react";
import ImgCat from "./ImgCat";

class Animal extends Component{
    constructor(props){
        super(props);
        this.state = {
            kind: "dog",
            
        }
    }
    render(){
        return (
            <>
                <ImgCat kindProps={this.state.kind}/>
            </>
        )
    }
}

export default Animal;