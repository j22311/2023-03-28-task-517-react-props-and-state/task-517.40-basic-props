import { Component } from "react";
import image from '../assets/images/cat.jpg'
class ImgCat extends Component{
    
    render(){
        let {kindProps} = this.props;
        return(
            kindProps ==="cat" ? <img src={image}></img> : <p>meow not found :</p>
        )
    }
}

export default ImgCat