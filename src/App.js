import logo from './logo.svg';
import './App.css';
import Animal from './components/Animal';

function App() {
  return (
    <div>
      <Animal/>
    </div>
  );
}

export default App;
